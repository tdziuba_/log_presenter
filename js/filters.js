'use strict';

/* Filters */

angular.module('LogPresenterFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});
