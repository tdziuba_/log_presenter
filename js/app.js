'use strict';

/* App Module */

var LogPresenter = angular.module('LogPresenter', [
  'ngRoute',
  'LogPresenterAnimations',

  'LogPresenterControllers',
  'LogPresenterFilters',
  'LogPresenterServices'
]);

LogPresenter.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/logs', {
        templateUrl: 'partials/log-list.html',
        controller: 'LogListCtrl'
      }).
      when('/logs/:logId', {
        templateUrl: 'partials/log-detail.html',
        controller: 'LogDetailCtrl'
      }).
      otherwise({
        redirectTo: '/logs'
      });
  }]);
