head.load(
  'lib/jquery/jquery-2.1.0.min.js',
  'lib/bootstrap/bootstrap.min.js',
  'lib/angular/angular.min.js',
  'lib/angular/angular-animate.min.js',
  'lib/angular/angular-resource.min.js',
  'lib/angular/angular-route.min.js'
);

head.ready(function() {
  head.load(
    'js/app.js',
    'js/animations.js',
    'js/controllers.js',
    'js/filters.js',
    'js/services.js'
  );
});