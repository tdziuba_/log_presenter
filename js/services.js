'use strict';

/* Services */

var LogPresenterServices = angular.module('LogPresenterServices', ['ngResource']);

LogPresenter.factory('Logs', ['$resource',
  function($resource) {
    return $resource('rest.php?:logId', {}, {
      query: {method:'GET', params:{logId: 'logs'}, isArray: false}
    });
  }]);

LogPresenter.factory('Log', ['$resource',
  function($resource) {
    return $resource('rest.php?log=:logId', {}, {
      query: {method:'GET', params:{logId: 'log'}, isArray: true}
    });
  }]);
