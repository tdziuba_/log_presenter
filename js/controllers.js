'use strict';

/* Controllers */

var LogPresenterControllers = angular.module('LogPresenterControllers', []);

LogPresenterControllers.controller('LogListCtrl', ['$scope', 'Logs',
  function($scope, Logs) {
    $scope.logs = Logs.query();
    $scope.orderProp = 'id';
  }]);

LogPresenterControllers.controller('LogDetailCtrl', ['$scope', '$routeParams', 'Log',
  function($scope, $routeParams, Log) {
    $scope.log = Log.get({logId: $routeParams.logId}, function(log) {
      $scope.log.i = 0;
    });
    $scope.collapse = function(number) {
      var items = jQuery('.panel-collapse');
      jQuery(items).removeClass('in');
      jQuery('.expand.expanded').removeClass('expanded');
      jQuery(items[number]).addClass('in');
      jQuery(items[number]).parent().find('.expand').addClass('expanded');
    };
  }]);
