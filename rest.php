<?php

if (isset($_GET['logs'])) {
  return Rest::get_log_list();
} elseif (isset($_GET['log'])) {
  return Rest::get_log_content($_GET['log']);
}

class Rest
{

  public static $instance;

  public static function get_instance()
  {
    if (!isset(self::$instance)) {
      self::$instance = new Rest();
    }
    return self::$instance;
  }

  private function __construct() {}

  private function __clone() {}

  public static function get_log_list()
  {
    $dir = 'logs';
    $files = scandir($dir);
    unset($files[0]);
    unset($files[1]);
    $i = 0;
    $map = array_map(function($file) use ($i) {
      return array(
        'no' => $i++,
        'id' => $file
      );
    }, $files);
    header('Content-Type: application/json');
    echo json_encode($map);
  }

  public static function get_log_content($log_name)
  {
    $obj = self::get_instance();
    $content = $obj->prepare_log(file_get_contents('logs/' . $log_name));

    $item = array(
      'id' => $log_name,
      'content' => $content
    );
    header('Content-Type: application/json');
    echo json_encode($item);
  }

  protected function prepare_log($content)
  {
//    $log_array = [];
//    $tmp = preg_split ('/$\R?^/m', $content);
//    $finished = array_map(function($rec) use ($log_array) {
//      return $this->push_to_log_array($rec);
//    }, $tmp);
//    return $finished;
    $log = array();
    $pattern = "/\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\].*/";

    preg_match_all($pattern, $content, $headings);
    $log_data = preg_split($pattern, $content);

    if ($log_data[0] < 1) {
        $trash = array_shift($log_data);
        unset($trash);
    }
    $i = -1;
    foreach ($headings[0] as $h) {
      $i++;
      $log[] = array('no'=>$i, 'heading'=>$h, 'data'=> $log_data[$i]);
    }

    unset($headings);
    unset($log_data);

    return $log;

  }

  protected function push_to_log_array($rec)
  {
    preg_match('/ERROR/', $rec, $matches, PREG_OFFSET_CAPTURE);
    $is_error = (count($matches > 0));
    preg_match('/INFO/', $rec, $matches, PREG_OFFSET_CAPTURE);
    $is_info = (count($matches > 0));
    if ($is_error) {
      return array('error' => $rec);
    } else if ($is_info) {
      return array('info' => $rec);
    } else {
      return array('notice' => $rec);
    }

  }

}

